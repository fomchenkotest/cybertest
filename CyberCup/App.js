import React from 'react';
import { View } from 'react-native';
import {
  createSwitchNavigator,
  createStackNavigator,
  createBottomTabNavigator,
  createAppContainer,
} from 'react-navigation';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import PlayButton from './src/components/PlayButton';

import AuthScreen from './src/screens/AuthScreen';
import HomeScreen from './src/screens/HomeScreen';
import GamesScreen from './src/screens/GamesScreen';
import PlayScreen from './src/screens/PlayScreen';
import FriendsScreen from './src/screens/FriendsScreen';
import YourProfileScreen from './src/screens/YourProfileScreen';
import SettingsScreen from './src/screens/SettingsScreen';

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#1b1b23',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  },
);

const GamesStack = createStackNavigator(
  {
    Games: GamesScreen,
  },
  {
    initialRouteName: 'Games',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#1b1b23',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  },
);

const PlayStack = createStackNavigator(
  {
    Play: PlayScreen,
  },
  {
    initialRouteName: 'Play',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#1b1b23',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  },
);

const FriendsStack = createStackNavigator(
  {
    Friends: FriendsScreen,
  },
  {
    initialRouteName: 'Friends',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#1b1b23',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  },
);

const YourProfileStack = createStackNavigator(
  {
    YourProfile: YourProfileScreen,
    Settings: SettingsScreen,
  },
  {
    initialRouteName: 'YourProfile',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#1b1b23',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  },
);

const TabNavigator = createBottomTabNavigator(
  {
    Home: {
      screen: HomeStack,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon
            name="home"
            color={tintColor}
            size={24}
          />
        ),
      },
    },
    Games: {
      screen: GamesStack,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon
            name="game-controller"
            color={tintColor}
            size={24}
          />
        ),
      },
    },
    Play: {
      screen: () => null,
      navigationOptions: ({ navigation }) => ({
        tabBarIcon: () => (
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: '#1b1b23',
              width: 100,
              height: 100,
              borderRadius: 50,
            }}
          >
            <PlayButton onPress={() => navigation.navigate('Modal')} />
          </View>
        ),
      }),
    },
    Friends: {
      screen: FriendsStack,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon
            name="heart"
            color={tintColor}
            size={24}
          />
        ),
      },
    },
    YourProfile: {
      screen: YourProfileStack,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon
            name="user"
            color={tintColor}
            size={24}
          />
        ),
      },
    },
  },
  {
    tabBarOptions: {
      showLabel: false,
      activeTintColor: '#835ee5',
      inactiveTintColor: '#fff', // or #3a3949
      style: {
        backgroundColor: '#1b1b23',
      },
    },
  },
);

const MainStack = createStackNavigator(
  {
    Main: TabNavigator,
    Modal: PlayStack,
  },
  {
    mode: 'modal',
    headerMode: 'none',
  },
);

const RootStack = createSwitchNavigator(
  {
    Auth: AuthScreen,
    App: MainStack,
  },
  {
    initialRouteName: 'Auth',
  },
);

const AppContainer = createAppContainer(RootStack);

const App = () => (
  <AppContainer />
);

export default App;
